/* -*- c++ -*- */
/* 
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */


#ifndef INCLUDED_USB_SOURCE_MY_USBSOURCE_H
#define INCLUDED_USB_SOURCE_MY_USBSOURCE_H

#include <usb_source/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
  namespace usb_source {

    /*!
     * \brief <+description of block+>
     * \ingroup usb_source
     *
     */
    class USB_SOURCE_API my_usbsource : virtual public gr::sync_block
    {
     public:
      typedef boost::shared_ptr<my_usbsource> sptr;

      /*!
       * \brief Return a shared_ptr to a new instance of usb_source::my_usbsource.
       *
       * To avoid accidental use of raw pointers, usb_source::my_usbsource's
       * constructor is in a private implementation
       * class. usb_source::my_usbsource::make is the public interface for
       * creating new instances.
       */
      static sptr make();
    };

  } // namespace usb_source
} // namespace gr

#endif /* INCLUDED_USB_SOURCE_MY_USBSOURCE_H */

