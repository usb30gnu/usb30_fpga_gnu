/* -*- c++ -*- */
/* 
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_USB_SOURCE_MY_USBSOURCE_IMPL_H
#define INCLUDED_USB_SOURCE_MY_USBSOURCE_IMPL_H

#include <usb_source/my_usbsource.h>
#include <string>
#include "libusb-1.0/libusb.h"

namespace gr {
  namespace usb_source {
	union usb_data_buffer{
	unsigned char data[516];
	int32_t number;
	}usb_buffer;

    class my_usbsource_impl : public my_usbsource
    {
     private:
      // Nothing to declare in this block.
 
	
	int err;
	int transfer_size;
	
	libusb_device_handle* dev;

     public:
      my_usbsource_impl();
      ~my_usbsource_impl();

      // Where all the action really happens
      int work(int noutput_items,
         gr_vector_const_void_star &input_items,
         gr_vector_void_star &output_items);
    };

  } // namespace usb_source
} // namespace gr

#endif /* INCLUDED_USB_SOURCE_MY_USBSOURCE_IMPL_H */

