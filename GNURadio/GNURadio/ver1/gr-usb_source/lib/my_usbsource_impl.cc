/* -*- c++ -*- */
/* 
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "my_usbsource_impl.h"

#define VENDOR_ID  0x04b4
#define PRODUCT_ID 0x00f1 
namespace gr {
  namespace usb_source {

    my_usbsource::sptr
    my_usbsource::make()
    {
      return gnuradio::get_initial_sptr
        (new my_usbsource_impl());
    }

    /*
     * The private constructor
     */
    my_usbsource_impl::my_usbsource_impl()
      : gr::sync_block("my_usbsource",
              gr::io_signature::make(0, 0, 0),  	  //The number of input port is made as 0 as the block is a source block
              gr::io_signature::make(1, 1, sizeof(float)))//We have used only one output port but the the number can be decided as per requirement
    {
	err=0;  		// the error status is initialized to zero 
	transfer_size=0;	// the variable which holds the size of the data transferred  
	dev=NULL;   		// the device handle is initialiased to NULL
	libusb_init(&ct);	// the libusb context is initiated. This helps the program to identify which devices is using the resources when many usb devices are connected.	
	libusb_set_debug(ct,LIBUSB_LOG_LEVEL_WARNING); // The debug state is set
 c=0;
	usb_bufferOut.number[0]=1;
	for(int i=1;i<16;++i)
{	
	usb_bufferOut.number[i]=31;
}
	dev=libusb_open_device_with_vid_pid(ct,VENDOR_ID,PRODUCT_ID);//The specified device is openned	
	try
{
	if(dev==NULL)
	{
		throw std::runtime_error("No devices specified was found"); // exception thrown when the specified device is not connected
	
	}else
	{
		// do nothing	
	}
}
	catch ( std::exception &ex ) {
        std::cerr << std::endl << "FATAL: " << ex.what() << std::endl << std::endl;//Thrown exception is caught here
	}
       

try
{
	err=libusb_detach_kernel_driver(dev,0);		// detach the libusb driver fromthe Linus kernel

	err=libusb_claim_interface(dev,0); 		//The connected specific device is claimed for data transfer

	if(err) 					//Check if the device is claimed successfully.
{
	throw std::runtime_error("device not claimed"); // Throw the error if device was unable to be claimed.
}	
	err=libusb_set_interface_alt_setting(dev,0,0); //Alternate setting is set

	if(err)						//Check if the alternate setting is set 
{	
	throw std::runtime_error("Alt setting not done");//Throw the error if alternate setting is not done
}	


	err=libusb_bulk_transfer(dev,0x01,(unsigned char*)usb_bufferOut.number,512,&transfer_size,1000);//The intializing Start sequence is sent to the FPGA through FX3.

if(err)
{
	throw std::runtime_error("BULK out failed");
}

}
catch ( std::exception &ex ) {
        std::cerr << std::endl << "FATAL: " << ex.what() << std::endl << std::endl;
	}
    }

    /*
     * Our virtual destructor.
     */
    my_usbsource_impl::~my_usbsource_impl()
    {
    }

    int
    my_usbsource_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
       float *out = (float *) output_items[0];
	float count=0;
	
      noutput_items=127;
	err=libusb_bulk_transfer(dev,0x81,(unsigned char*)usb_buffer.number,512,&transfer_size,1000);
	if(err)
	{
	throw std::runtime_error("bulk transfer failed");	
	}
		
	for (int i=0; i<=noutput_items;++i)
	{
		out[i]=usb_buffer.number[i];
		
	}
	
      // Tell runtime system how many output items we produced.
	
      return noutput_items;
    }

  } /* namespace usb_source */
} /* namespace gr */

