INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_USB_SOURCE usb_source)

FIND_PATH(
    USB_SOURCE_INCLUDE_DIRS
    NAMES usb_source/api.h
    HINTS $ENV{USB_SOURCE_DIR}/include
        ${PC_USB_SOURCE_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    USB_SOURCE_LIBRARIES
    NAMES gnuradio-usb_source
    HINTS $ENV{USB_SOURCE_DIR}/lib
        ${PC_USB_SOURCE_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(USB_SOURCE DEFAULT_MSG USB_SOURCE_LIBRARIES USB_SOURCE_INCLUDE_DIRS)
MARK_AS_ADVANCED(USB_SOURCE_LIBRARIES USB_SOURCE_INCLUDE_DIRS)

