/* -*- c++ -*- */

#define USB_SOURCE_API

%include "gnuradio.i"			// the common stuff

//load generated python docstrings
%include "usb_source_swig_doc.i"

%{
#include "usb_source/my_usbsource.h"
%}


%include "usb_source/my_usbsource.h"
GR_SWIG_BLOCK_MAGIC2(usb_source, my_usbsource);
