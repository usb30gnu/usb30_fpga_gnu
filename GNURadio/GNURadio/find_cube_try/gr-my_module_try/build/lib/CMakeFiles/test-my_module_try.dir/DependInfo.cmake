# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/vivek/code/gr-my_module_try/lib/qa_my_module_try.cc" "/home/vivek/code/gr-my_module_try/build/lib/CMakeFiles/test-my_module_try.dir/qa_my_module_try.cc.o"
  "/home/vivek/code/gr-my_module_try/lib/test_my_module_try.cc" "/home/vivek/code/gr-my_module_try/build/lib/CMakeFiles/test-my_module_try.dir/test_my_module_try.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../lib"
  "../include"
  "lib"
  "include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/vivek/code/gr-my_module_try/build/lib/CMakeFiles/gnuradio-my_module_try.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
