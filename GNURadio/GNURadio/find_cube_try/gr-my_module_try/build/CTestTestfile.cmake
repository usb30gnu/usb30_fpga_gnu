# CMake generated Testfile for 
# Source directory: /home/vivek/code/gr-my_module_try
# Build directory: /home/vivek/code/gr-my_module_try/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(include/my_module_try)
subdirs(lib)
subdirs(swig)
subdirs(python)
subdirs(grc)
subdirs(apps)
subdirs(docs)
