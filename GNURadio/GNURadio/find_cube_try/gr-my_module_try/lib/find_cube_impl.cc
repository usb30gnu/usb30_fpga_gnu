/* -*- c++ -*- */
/* 
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "find_cube_impl.h"

namespace gr {
  namespace my_module_try {

    find_cube::sptr
    find_cube::make()
    {
      return gnuradio::get_initial_sptr
        (new find_cube_impl());
    }

    /*
     * The private constructor
     */
    find_cube_impl::find_cube_impl()
      : gr::block("find_cube",
              gr::io_signature::make(1, 1, sizeof(float)),//input
              gr::io_signature::make(1, 1, sizeof(float)))//output
    {

	input =0;// intializing the input
	output =0;// intializing the output
	set_max_noutput_items(1);
	set_max_output_buffer(2);

    }

    /*
     * Our virtual destructor.
     */
    find_cube_impl::~find_cube_impl()
    {
    }

    void
    find_cube_impl::forecast (int noutput_items, gr_vector_int &ninput_items_required)
    {
      /* <+forecast+> e.g. ninput_items_required[0] = noutput_items */
	ninput_items_required[0] = noutput_items;// number of input required for the output to be processed
    }

    int
    find_cube_impl::general_work (int noutput_items,
                       gr_vector_int &ninput_items,
                       gr_vector_const_void_star &input_items,
                       gr_vector_void_star &output_items)
    {
      const float *in = (const float *) input_items[0];
      float *out = (float *) output_items[0];

      input =in[0];
      output= input*input*input;
      out[0]=output;	
	
      consume_each (noutput_items);

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace my_module_try */
} /* namespace gr */

