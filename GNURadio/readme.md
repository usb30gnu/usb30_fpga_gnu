Validation of the received sine wave from FPGA to the GNU Platform

After the communication is established in the system, a sine wave is sent from FPGA to the GNU platform. To validate the sine wave signal received, there are many different ways and here we proceed with auto correlation concept. The objective here is to find out the periodicity and amplitude of the received signal and compare it with the properties of the signal transmitted from FPGA.
Pre requisite: The sine wave received at GNU platform is seen on graph. Points from the sine wave are saved in a text file. A text file with all the sine wave points, separated by a comma is used as an input for the validation process.

Tool: Eclipse (C code)

Concept Used: Auto correlation
Auto correlation is the correlation of a signal with a delayed copy of itself as a function of delay. It is the similarity between observations as a function of the time lag between them. Autocorrelation Function tells you how the points are correlated with each other, based on how many time steps they are separated by. It is how correlated past data points are to future data points, for different values of the time separation. The analysis of autocorrelation is a mathematical tool for finding repeated patterns, such as presence of periodicity in the delayed signal. The autocorrelation function can be used for the following two purposes:
•	To detect non randomness in data.
•	To identify an appropriate time series model if the data are not random.
Steps followed in c code to validate the signal:
•	Read the values from the text file to an array.
•	Compute the mean and variance using mathematical formula.  Mean for i-th element of a series with N element would be defined as,
 mean = mean + (X[i] / N) 
and variance for the series would be defined as, 
variance = variance +(pow((X[i] - Mean), 2.0) / N)
•	Calculate auto correlation using auto-covariance and variance using the following formula, 
acv = acv + ((X[i] - Mean) * (X[i+lag] - Mean)) .
•	Finally autocorrelation arc,
ARC = acv / variance
•	Using the auto correlation and tolerance concept we find the periodicity of the signal.
•	 Tolerance is a measured value that has some variation without significantly affecting functioning of systems. A variation beyond the tolerance is said to be noncompliant, rejected, or exceeding the tolerance. Since it is very difficult to get the exact time for the repetition of the signal, we have taken a tolerance value and calculated the auto correlation value.
•	 Using this concept we find after how many time lags the signal repeats itself. This lag is used to find the period of the signal. 
•	If Period is denoted by P and sampling frequency of the signal is denoted by fs then,     P = lagperiod/fs.
•	Compare it with the periodicity of the transmitted signal.
•	Using another c function find the maximum and minimum amplitude of the signal and compare it with the transmitted signal.

Since the periodicity and the amplitude of the received signal matches with the transmitted signal, we successfully validate that the signal transmitted from the FPGA has been received correctly on the GNU platform.

