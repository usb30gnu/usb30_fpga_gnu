// SerialRx.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <Windows.h>
#include <iostream>
#include <stdio.h>
#include "inc\CyAPI.h"
#include <cstring>
using namespace std;

#define     VENDOR_CMD_START_TRANSFER           0xB5
#define     BULK_END_POINT                      2
#define		TIMEOUT_PER_TRANSFER_MILLI_SEC		1500

void getData()
{
	CCyUSBDevice	*USBDevice;
	CCyUSBEndPoint	*usb_ep = NULL;
	long recievedBytes;
	int QueueSize  = 1;
	PUCHAR			*buffers = new PUCHAR[QueueSize];
	PUCHAR			*contexts = new PUCHAR[QueueSize];
	OVERLAPPED		overlap_in[64];
	int             index = 0;
	HICON m_hIcon;
	HDEVNOTIFY *m_hDeviceNotify;
	
	//Initialize the USB device
	USBDevice = new CCyUSBDevice(0, CYUSBDRV_GUID, true);

	if (USBDevice != NULL)
	{
		//Count
		int nDeviceCount = USBDevice->DeviceCount();
		std::cout << "The number of devices " << nDeviceCount << std::endl;
		for (int index = 0; index < nDeviceCount; index++)
		{
			string strDeviceData;
			USBDevice->Open(index);
			cout << "USB Vendor ID : " << USBDevice->VendorID << "Prod ID : " << USBDevice->ProductID << "Friendly name : " << USBDevice->FriendlyName << endl;
		}

		//Get Endpoints
		UCHAR nEndPointCount = USBDevice->EndPointCount();

		for (index = 0; index < nEndPointCount; index++)
		{
			usb_ep = USBDevice->EndPoints[index];
			if (usb_ep->Attributes == BULK_END_POINT && usb_ep->bIn == true)
				break;
		}

		long totalTransferSize = usb_ep->MaxPktSize * QueueSize;
		usb_ep->SetXferSize(totalTransferSize);

		// Allocate all the buffers for the queues
		for (index = 0; index < QueueSize; index++)
		{
			buffers[index] = new UCHAR[totalTransferSize];
			overlap_in[index].hEvent = CreateEvent(NULL, false, false, NULL);
			memset(buffers[index], 0xEF, totalTransferSize);
		}


		for (index = 0; index < QueueSize; index++)
		{
			contexts[index] = usb_ep->BeginDataXfer(buffers[index], totalTransferSize, &overlap_in[index]);
			if (usb_ep->NtStatus || usb_ep->UsbdStatus)
			{

				return;
			}
		}

		if (USBDevice->bSuperSpeed)
		{
			LONG dataLength = 0;
			CCyControlEndPoint *ControlEndPt = (CCyControlEndPoint *)USBDevice->EndPointOf(0);

			ControlEndPt->Target = TGT_DEVICE;
			ControlEndPt->ReqType = REQ_VENDOR;
			// Vendor Command that is transmitted for starting the read.
			ControlEndPt->ReqCode = VENDOR_CMD_START_TRANSFER;
			ControlEndPt->Direction = DIR_TO_DEVICE;
			//// Send Value = 1 and Index = 0, to kick start the transaction.
			ControlEndPt->Value = 0x0001;
			ControlEndPt->Index = 0;

			// Send vendor command now......
			ControlEndPt->XferData(NULL, dataLength, NULL);
		}

		//Data collection start
		index = 0;
		recievedBytes = 0;

		while (1)
		{
			long readLength = totalTransferSize;

			// Error timeout check
			if (!usb_ep->WaitForXfer(&overlap_in[index], TIMEOUT_PER_TRANSFER_MILLI_SEC))
			{
				usb_ep->Abort();
				if (usb_ep->LastError == ERROR_IO_PENDING)
					WaitForSingleObject(overlap_in[index].hEvent, TIMEOUT_PER_TRANSFER_MILLI_SEC);
			}

			//Reading the dat
			if (usb_ep->FinishDataXfer(buffers[index], readLength, &overlap_in[index], contexts[index]))
				recievedBytes += totalTransferSize;

			if (recievedBytes < 0) // Rollover - reset counters
			{
				recievedBytes = 0;
			}

			cout << "Data Recieved : ";
			for (int i = 0; i < readLength; i++)
				printf("%x  ", buffers[index][i]);

			cout << "Bytes Recieved : " << recievedBytes << endl;


			// Re-submit this queue element to keep the queue full
			contexts[index] = usb_ep->BeginDataXfer(buffers[index], totalTransferSize, &overlap_in[index]);
			if (usb_ep->NtStatus || usb_ep->UsbdStatus)
			{
				usb_ep->Abort();
			}
		}
	}
}

int main()
{	
	getData();

	return 0;
}

