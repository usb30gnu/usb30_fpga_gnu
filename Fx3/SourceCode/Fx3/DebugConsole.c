/*
 * DebugConsole.c
 *
 *  Created on: Feb 18, 2014
 *      Author: John
 */

#include "Application.h"

// Declare external functions
extern void CheckStatus(char* StringPtr, CyU3PReturnStatus_t Status);
extern void StartApplication(void);
extern void StopApplication(void);
extern void CyU3PDebugPrintEvent(uint32_t ActualEvents);
extern void ParseCommand(void);

CyBool_t glDebugTxEnabled = CyFalse;	// Set true once I can output messages to the Console
CyU3PDmaChannel glUARTtoCPU_Handle;		// Handle needed by Uart Callback routine
char glConsoleInBuffer[32];				// Buffer for user Console Input
uint32_t glConsoleInIndex;				// Index into ConsoleIn buffer
uint32_t ClockValue;
extern CyU3PEvent CallbackEvent;
uint16_t MAXCLOCKVALUE = 10;

void UartCallback(CyU3PUartEvt_t Event, CyU3PUartError_t Error)
// Handle characters typed in by the developer
// Look for and execute commands on a <CR>
{
	CyU3PDmaBuffer_t ConsoleInDmaBuffer;
	char InputChar;
	if (Event == CY_U3P_UART_EVENT_RX_DONE)
	{
		CyU3PDmaChannelSetWrapUp(&glUARTtoCPU_Handle);
		CyU3PDmaChannelGetBuffer(&glUARTtoCPU_Handle, &ConsoleInDmaBuffer, CYU3P_NO_WAIT);
		InputChar = (char)*ConsoleInDmaBuffer.buffer;
		CyU3PDebugPrint(4, "%c", InputChar);			// Echo the character
		// On CR, signal Main loop to process the command entered by the user.  Should NOT do this in a CallBack routine
		if (InputChar == 0x0d) CyU3PEventSet(&CallbackEvent, USER_COMMAND_AVAILABLE, CYU3P_EVENT_OR);
		else
		{
			glConsoleInBuffer[glConsoleInIndex] = InputChar | 0x20;		// Save character as lower case (for compares)
			if (glConsoleInIndex++ < sizeof(glConsoleInBuffer)) glConsoleInBuffer[glConsoleInIndex] = 0;
			else glConsoleInIndex--;
		}
		CyU3PDmaChannelDiscardBuffer(&glUARTtoCPU_Handle);
		CyU3PUartRxSetBlockXfer(1);
	}
}

// Spin up the DEBUG Console, Out and In
CyU3PReturnStatus_t InitializeDebugConsole(void)
{
    CyU3PUartConfig_t uartConfig;
    CyU3PDmaChannelConfig_t dmaConfig;
    CyU3PReturnStatus_t Status = CY_U3P_SUCCESS;

    Status = CyU3PUartInit();										// Start the UART driver
    CheckStatus("CyU3PUartInit", Status);							// Note that this won't display since console is not up yet

    CyU3PMemSet ((uint8_t *)&uartConfig, 0, sizeof (uartConfig));
	uartConfig.baudRate = CY_U3P_UART_BAUDRATE_115200;
	uartConfig.stopBit  = CY_U3P_UART_ONE_STOP_BIT;
	uartConfig.parity   = CY_U3P_UART_NO_PARITY;
	uartConfig.txEnable = CyTrue;
	uartConfig.rxEnable = CyTrue;
	uartConfig.flowCtrl = CyFalse;
	uartConfig.isDma    = CyTrue;

	Status = CyU3PUartSetConfig(&uartConfig, UartCallback);			// Configure the UART hardware
    CheckStatus("CyU3PUartSetConfig", Status);

    // TX configuration
    Status = CyU3PUartTxSetBlockXfer(0xFFFFFFFF);					// Send as much data as I need to
    CheckStatus("CyU3PUartTxSetBlockXfer", Status);

	Status = CyU3PDebugInit(CY_U3P_LPP_SOCKET_UART_CONS, 8);		// Attach the Debug driver above the UART driver
	if (Status == CY_U3P_SUCCESS) glDebugTxEnabled = CyTrue;		// Console Out is now up :-)
    CheckStatus("ConsoleOutEnabled", Status);

    CyU3PDebugPreamble(CyFalse);									// Skip preamble, debug info is targeted for a person

	// Rx Configuration
    Status = CyU3PUartRxSetBlockXfer(1);
    CheckStatus("CyU3PUartRxSetBlockXfer", Status);
	CyU3PMemSet((uint8_t *)&dmaConfig, 0, sizeof(dmaConfig));
	dmaConfig.size  		= 16;									// Minimum size allowed, I only need 1 byte
	dmaConfig.count 		= 1;									// I can't type faster than the Uart Callback routine!
	dmaConfig.prodSckId		= CY_U3P_LPP_SOCKET_UART_PROD;
	dmaConfig.consSckId 	= CY_U3P_CPU_SOCKET_CONS;
	dmaConfig.dmaMode 		= CY_U3P_DMA_MODE_BYTE;
	dmaConfig.notification	= CY_U3P_DMA_CB_PROD_EVENT;
	Status = CyU3PDmaChannelCreate(&glUARTtoCPU_Handle, CY_U3P_DMA_TYPE_MANUAL_IN, &dmaConfig);
    CheckStatus("CreateDebugRxDmaChannel", Status);
    if (Status != CY_U3P_SUCCESS) CyU3PDmaChannelDestroy(&glUARTtoCPU_Handle);
    else
    {
		Status = CyU3PDmaChannelSetXfer(&glUARTtoCPU_Handle, 0);
		CheckStatus("ConsoleInEnabled", Status);
    }
    return Status;
}

const char* EventName[] = {
	    "CONNECT", "DISCONNECT", "SUSPEND", "RESUME", "RESET", "SET_CONFIGURATION", "SPEED",
	    "SET_INTERFACE", "SET_EXIT_LATENCY", "SOF_ITP", "USER_EP0_XFER_COMPLETE", "VBUS_VALID",
	    "VBUS_REMOVED", "HOSTMODE_CONNECT", "HOSTMODE_DISCONNECT", "OTG_CHANGE", "OTG_VBUS_CHG",
	    "OTG_SRP", "EP_UNDERRUN", "LINK_RECOVERY", "USB3_LINKFAIL", "SS_COMP_ENTRY", "SS_COMP_EXIT"
};


void DebugPrintEvent(uint32_t ActualEvents)
{
	uint32_t Index = 0;
	ActualEvents &= USB_EVENTS;
	while (ActualEvents)
	{
		if (ActualEvents & 1) CyU3PDebugPrint(4, "\r\nEvent received = %s\r\n", EventName[Index]);
		Index++;
		ActualEvents >>= 1;
	}
}

void ParseCommand(void)
{
	// User has entered a command, process it
    CyU3PReturnStatus_t Status = CY_U3P_SUCCESS;

    if (strncmp("pclk", glConsoleInBuffer, 4) == 0)
	{
		if (glConsoleInBuffer[4] == '-') ClockValue++;
		// For CPLD board, with a -7 XC2C128 the maximum frequency with margin is 80MHz
		if ((glConsoleInBuffer[4] == '+') && (ClockValue > MAXCLOCKVALUE)) ClockValue--;
		// It is not sufficient to Stop and Restart the GPIF clocks since the DMA buffers waiting at GPIF get confused
		// Safest is to STOP and START the application again so that the DMA channel is re-initialized
		// Need to RESET the CPLD since it will move to an unknown state with no clocks
		Status = CyU3PGpioSetValue(CPLD_RESET, 1);	 	// Drive CPLD_RESET
    	CheckStatus("Reset CPLD", Status);
    	// Temporarily turn off "Successful" messages while application restarts
    	CyU3PDebugSetTraceLevel(6);
		StopApplication();
		CyU3PDebugPrint(4, "\r\nRestarting application");
		StartApplication();
    	CyU3PDebugSetTraceLevel(8);
	}
	else if (!strcmp("threads", glConsoleInBuffer))
	{
		CyU3PThread *ThisThread, *NextThread;
		char* ThreadName;
		// First find out who I am
		ThisThread = CyU3PThreadIdentify();
		tx_thread_info_get(ThisThread, &ThreadName, NULL, NULL, NULL, NULL, NULL, &NextThread, NULL);
		// Now, using the Thread linked list, look for other threads until I find myself again
		while (NextThread != ThisThread)
		{
			tx_thread_info_get(NextThread, &ThreadName, NULL, NULL, NULL, NULL, NULL, &NextThread, NULL);
			CyU3PDebugPrint(4, "\r\nFound: '%s'", ThreadName);
		}
	}
	else if (!strcmp("reset", glConsoleInBuffer))
	{
		CyU3PDebugPrint(4, "\r\nRESETTING CPU\r\n");
		CyU3PThreadSleep(100);
		CyU3PDeviceReset(CyFalse);
	}
	else if (!strcmp("cpld", glConsoleInBuffer))
	{
		CyBool_t State;
		CyU3PGpioSimpleGetValue(CPLD_RESET, &State);
		State = !State;
		CyU3PGpioSetValue(CPLD_RESET, State);
		CyU3PDebugPrint(4, "\r\nToggle CPLD RESET, now = %d\r\n", State);
	}
	else if (!strcmp("gpif", glConsoleInBuffer))
	{
		uint8_t State = 0xFF;
		Status = CyU3PGpifGetSMState(&State);
		CheckStatus("Get GPIF State", Status);
		CyU3PDebugPrint(4, "\r\nGPIF State = %d\r\n", State);
	}
	else CyU3PDebugPrint(4, "Input: '%s'\r\n", &glConsoleInBuffer[0]);
    glConsoleInIndex = 0;
}

