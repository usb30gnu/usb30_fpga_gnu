/**
 * @file	RunApplication.c
 * @brief	Running the OS application threads
 *
 * @author	Vinurudh Damarla
 *
 */
#include "Application.h"

/**
 * @brief <b> Setting up the USB Descriptors </b><br>
 *
 * @return status CyU3PReturnStatus_t, the status of the configuration
 */
extern CyU3PReturnStatus_t InitializeDebugConsole(void);

/**
 * @brief <b> Setting up the USB Descriptors </b><br>
 *
 * @return status CyU3PReturnStatus_t, the status of the configuration
 */
extern CyU3PReturnStatus_t InitializeUSB(void);

/**
 * @brief <b> Checks the status </b><br>
 * Checks the status of the API and prints in the debug console
 *
 * @param StringPtr char, name to be printed
 * @param status CyU3PReturnStatus_t, the status returned
 */
extern void CheckStatus(char* StringPtr, CyU3PReturnStatus_t Status);

/**
 * @brief <b> Printing the event received </b><br>
 *
 * @param ActualEvents uint32_t, event received
 */
extern void DebugPrintEvent(uint32_t ActualEvents);

/**
 * @brief <b> Printing the event received </b><br>
 *
 * @param ActualEvents uint32_t, event received
 */
extern void ParseCommand(void);
void InitializeCPLD(void);

CyU3PThread ApplicationThread;			// Handle to my Application Thread
CyU3PEvent glApplicationEvent;			// An Event to allow threads to communicate

CyU3PThread ThreadHandle[2];
CyBool_t glIsApplicationActive;		// Set true once device is enumerated
CyU3PEvent CallbackEvent;
uint16_t Delay;

// Call back function for GPIO
void Gpio_IntrCB(uint8_t gpioId)
{
	if(gpioId == BUTTON)
	{
		Delay = (Delay == 1000) ? 100 : 1000;
		CyU3PDebugPrint(4, "\r\nButton Press Detected \r\n");
	}
}

void InitializeDevices()
{
	 uint32_t Status;

	// Create any needed resources then the Application thread
	Status = InitializeDebugConsole();
	CheckStatus("Initialize Debug Console", Status);

	// Create an Event so that alerts from CallBack routines can be monitored
	Status = CyU3PEventCreate(&CallbackEvent);
	CheckStatus("Create CallbackEvent", Status);

	// Initialize CPLD / FPGA
#if CPLD_CONNECTED
	//InitializeCPLD();
#endif

#if FPGA_CONNECTED
	//InitializeFPGA();
#endif

	// Initialize the USB
	Status = InitializeUSB();
	CheckStatus("Initialize USB", Status);
}

void InitializeCPLD(void)
{
	CyU3PReturnStatus_t Status;
	CyU3PGpioClock_t GpioClock;
	CyU3PGpioSimpleConfig_t gpioConfig;


	// Startup the GPIO module clocks
	GpioClock.fastClkDiv = 2;
	GpioClock.slowClkDiv = 0;
	GpioClock.simpleDiv = CY_U3P_GPIO_SIMPLE_DIV_BY_2;
	GpioClock.clkSrc = CY_U3P_SYS_CLK;
	GpioClock.halfDiv = 0;
	Status = CyU3PGpioInit(&GpioClock, Gpio_IntrCB);
	CheckStatus("Start GPIO Clocks", Status);

	// Need to claim CTRL[10] from the GPIF Interface
	Status = CyU3PDeviceGpioOverride(CPLD_RESET, CyTrue);
	CheckStatus("CPLD_RESET Override", Status);

	// Reset by driving CPLD_RESET High
	CyU3PMemSet((uint8_t *)&gpioConfig, 0, sizeof(gpioConfig));
	gpioConfig.outValue = 1;
	gpioConfig.driveLowEn = CyTrue;
	gpioConfig.driveHighEn = CyTrue;
	Status = CyU3PGpioSetSimpleConfig(CPLD_RESET, &gpioConfig);
	CheckStatus("Reset CPLD", Status);
}

void ApplicationThread_Entry (uint32_t Value)
{
	int32_t Seconds = 0;
    CyU3PReturnStatus_t Status,Count;
    uint32_t ActualEvents;
    char* ThreadName;

#if CPLD_CONNECTED
    Status = CyU3PGpioSetValue(CPLD_RESET,CyFalse);
    CheckStatus("Disable Reset CPLD", Status);
#endif
    // Create an Event which will allow the different threads/modules to synchronize
    Status = CyU3PEventCreate(&glApplicationEvent);
    CheckStatus("Create Event", Status);

    if (Status == CY_U3P_SUCCESS)
    {
    	CyU3PThreadInfoGet(&ApplicationThread, &ThreadName, 0, 0, 0);
    	ThreadName += 3;	// Skip numeric ID
    	CyU3PDebugPrint(4, "\r\n%s started with %d", ThreadName, Value);
    	// Now run forever

    	while (!glIsApplicationActive)
    	{
    		// Check for USB CallBack Events every 100msec
    		Status = CyU3PEventGet(&CallbackEvent, USB_EVENTS, CYU3P_EVENT_OR_CLEAR, &ActualEvents, 100);
    		if (Status == TX_SUCCESS) DebugPrintEvent(ActualEvents);
    	}

    	while (1)
    	{
    		for (Count = 0; Count<10; Count++)
    		{
    			// Check for User Commands (and other CallBack Events) every 100msec
    			CyU3PThreadSleep(100);
    			Status = CyU3PEventGet(&CallbackEvent, ANY_EVENT, CYU3P_EVENT_OR_CLEAR, &ActualEvents, TX_NO_WAIT);
    			if (Status == TX_SUCCESS)
    			{
    				if (ActualEvents & USER_COMMAND_AVAILABLE) ParseCommand();
    				else DebugPrintEvent(ActualEvents);
    			}
    		}
    		CyU3PDebugPrint(4, "%d, ", Seconds++);
		}
    }
    CyU3PDebugPrint(4, "\r\nApplication failed to initialize. Error code: %d.\r\n", Status);
    while (1);		// Hang here
}


void LedBlinkingThread(uint32_t Value)
{
	uint8_t Counter = 0; //Counter for the blinking
	CyU3PGpioSimpleConfig_t GpioConfig;
	CyU3PGpioClock_t GpioClock;
	CyU3PReturnStatus_t Status;

	GpioClock.fastClkDiv = 2;
	GpioClock.slowClkDiv = 0;
	GpioClock.simpleDiv = CY_U3P_GPIO_SIMPLE_DIV_BY_2;
	GpioClock.clkSrc = CY_U3P_SYS_CLK;
	GpioClock.halfDiv = 0;
	Status = CyU3PGpioInit(&GpioClock, Gpio_IntrCB);
	CheckStatus("Start GPIO Clocks", Status);

	CyU3PDeviceGpioOverride(LED,true);
	CyU3PMemSet((uint8_t *)&GpioConfig, 0, sizeof(GpioConfig));
	GpioConfig.driveHighEn = true;
	GpioConfig.driveLowEn = true;
	Status = CyU3PGpioSetSimpleConfig(LED,&GpioConfig);
	CheckStatus("LED Initialized", Status);

	CyU3PMemSet((uint8_t *)&GpioConfig, 0, sizeof(GpioConfig));
	GpioConfig.inputEn = true;
	GpioConfig.intrMode = CY_U3P_GPIO_INTR_NEG_EDGE;
	Status = CyU3PGpioSetSimpleConfig(BUTTON,&GpioConfig);
	CheckStatus("Button Initialized", Status);

	Delay = 1000;
	CyU3PThreadSleep(Delay);
	while(1)
	{
		CyU3PGpioSetValue(LED,(1 & Counter++));
		CyU3PThreadSleep(Delay);
	}
}

// ApplicationDefine function is called by RTOS to startup the application thread after it has initialized its own threads
void CyFxApplicationDefine(void)
{
    void *StackPtr = NULL;
    uint32_t Status;

    // Initialize the devices
    InitializeDevices();

    // Create the threads
    StackPtr = CyU3PMemAlloc(APPLICATION_THREAD_STACK);
    Status = CyU3PThreadCreate (&ApplicationThread, // Handle to my Application Thread
            "Slave FIFO FPGA",                			// Thread ID and name
            ApplicationThread_Entry,     			// Thread entry function
            0,                             		// Parameter passed to Thread
            StackPtr,                       		// Pointer to the allocated thread stack
            APPLICATION_THREAD_STACK,               // Allocated thread stack size
            APPLICATION_THREAD_PRIORITY,            // Thread priority
            APPLICATION_THREAD_PRIORITY,            // = Thread priority so no preemption
            CYU3P_NO_TIME_SLICE,            		// Time slice not supported in FX3 implementation
            CYU3P_AUTO_START                		// Start the thread immediately
            );

    StackPtr = CyU3PMemAlloc(APPLICATION_THREAD_STACK);
    Status = CyU3PThreadCreate(&ThreadHandle[1],
    		"LED Blinking Task",
    		LedBlinkingThread,
    		1,
    		StackPtr,
    		APPLICATION_THREAD_STACK,
    		APPLICATION_THREAD_PRIORITY,
    		APPLICATION_THREAD_PRIORITY,
    		CYU3P_NO_TIME_SLICE,
    		CYU3P_AUTO_START);

    if (Status != CY_U3P_SUCCESS)
    {
        /* Thread creation failed with the Status = error code */

        /* Add custom recovery or debug actions here */

        /* Application cannot continue. Loop indefinitely */
        while(1);
    }
}


