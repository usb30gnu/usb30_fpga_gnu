// This file contains the constants used by the application

#include "cyu3types.h"
#include "cyu3system.h"
#include "cyu3os.h"
#include "cyu3dma.h"
#include "cyu3error.h"
#include "cyu3usbconst.h"
#include "cyu3usb.h"
#include "cyu3uart.h"
#include "cyu3externcstart.h"
#include "cyu3pib.h"
#include "cyu3gpif.h"
#include "cyu3lpp.h"
#include "cyu3gpio.h"

#define USB_CONNECTION_ACTIVE		 1			// An Event Flag

#define STREAM_IN_ENABLED		1
#define STREAM_OUT_ENABLED		1

#define CPLD_CONNECTED			0
#define FPGA_CONNECTED			0

#define UART_CTS			(54)	// Also LED
#define UART_RTS			(53)	// Won't be used with UART so this gives me an extra GPIO to use
#define PUSH_BUTTON			(45)	// This example checks the PushButton and debounces it for the CPLD
#define CPLD_RESET			(27)	// Hold CPLD in RESET until I've set up and am ready to run
#define CPLD_PUSH_BUTTON	(26)	// Debounced PushButton
#define CPLD_LastRDData		(20)	// CPLD needs to tell me the last data on a READ

#define USB_EVENTS				(0x7FFFFF)
#define USER_COMMAND_AVAILABLE	(23)
#define ANY_EVENT				(USB_EVENTS + (1<<USER_COMMAND_AVAILABLE))

#define APPLICATION_THREAD_STACK	(0x1000)
#define APPLICATION_THREAD_PRIORITY	(8)

#define USB_PRODUCER_ENDPOINT			0x01
#define USB_CONSUMER_ENDPOINT			0x81    /* EP 1 IN */

#define USB_CONSUMER_ENDPOINT_SOCKET	CY_U3P_UIB_SOCKET_CONS_1
#define USB_PRODUCER_ENDPOINT_SOCKET	CY_U3P_UIB_SOCKET_PROD_1

#define GPIF_PRODUCER_SOCKET    		CY_U3P_PIB_SOCKET_0    /* P-port Socket 0 is producer */
#define GPIF_CONSUMER_SOCKET			CY_U3P_PIB_SOCKET_3

#define STANDARD_REQUEST	(0)			// My values are not shifted
#define CLASS_REQUEST		(1)
#define VENDOR_REQUEST		(2)

#define DMA_BUFFER_SIZE			(16384)
#define DMA_BUFFER_COUNT		(4)

#define ENDPOINT_BURST_LENGTH	(16)

#define LED 54
#define BUTTON 45
#define true CyTrue
#define false CyFalse


#include "cyu3externcend.h"
