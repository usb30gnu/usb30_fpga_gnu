/**
 * @file	USB_Handler.c
 * @brief	The USB setting and handling for the transmission
 *
 * @author	Vinurudh Damarla
 *
 */
#include "Application.h"

// Declare external functions
/**
 * @brief <b> Checks the status </b><br>
 * Checks the status of the API and prints in the debug console
 *
 * @param StringPtr char, name to be printed
 * @param status CyU3PReturnStatus_t, the status returned
 */
extern void CheckStatus(char* StringPtr, CyU3PReturnStatus_t Status);

/**
 * @brief <b> Setting up the USB Descriptors </b><br>
 *
 * @return status CyU3PReturnStatus_t, the status of the configuration
 */
extern CyU3PReturnStatus_t SetUSBdescriptors(void);

/**
 * @brief <b> Initialize and configure USB, DMA and GPIF start the transfer </b><br>
 */
extern void StartApplication(void);

/**
 * @brief <b> Deinitialize USB, DMA and GPIF start the transfer </b><br>
 */
extern void StopApplication(void);

// Declare external data
extern CyU3PEvent CallbackEvent;			/**< Used for events detected in CallBack routines */
extern CyBool_t glIsApplicationActive;		/**< Set true once device is enumerated */
extern uint32_t ClockValue;					/**< Used to select GPIF speed */
extern uint32_t MAXCLOCKVALUE;				/**< clock value */

// Global data owned by this module
volatile uint8_t  glVendorRequestCount = 0;
volatile uint32_t glUnderrunCount = 0;
uint8_t glEp0Buffer[32] __attribute__ ((aligned (32))); /* Local buffer used for vendor command handling. */
CyBool_t glForceLinkU2;
CyBool_t glChannelSuspended;


// Declare the callbacks needed to support the USB device driver
/**
 * @brief <b> USB Callbacks </b><br>
 * callbacks needed to support the USB device driver
 *
 * @param setupdat0 uint32_t
 * @param setupdat1 uint32_t
 */
CyBool_t USBSetup_Callback(uint32_t setupdat0, uint32_t setupdat1)
{
	uint8_t  request, reqType, type, target ;
	uint16_t length, index, value;
	CyBool_t isHandled = CyFalse;

	//extract data fields

	reqType =  	(setupdat0 & CY_U3P_USB_REQUEST_TYPE_MASK);
	type 	=	(reqType & CY_U3P_USB_TYPE_MASK);
	target	=	(reqType & CY_U3P_USB_TARGET_MASK);
	request	=	((setupdat0 & CY_U3P_USB_REQUEST_MASK) >> CY_U3P_USB_REQUEST_POS);
	value	=	((setupdat0 & CY_U3P_USB_VALUE_MASK)   >> CY_U3P_USB_VALUE_POS);
	index	=	((setupdat1 & CY_U3P_USB_INDEX_MASK)   >> CY_U3P_USB_INDEX_POS);
	length 	= 	((setupdat1 & CY_U3P_USB_LENGTH_MASK)  >> CY_U3P_USB_LENGTH_POS);

	// USB Driver will send me Class and Vendor requests to handle
    if (type == VENDOR_REQUEST)
    {
        if (request == 0xB5)
        {
        	CyU3PDebugPrint(4, "\r\nGot Command = %s Data Collection", value ? "Start" : "Stop");
        	CyU3PUsbAckSetup();
            isHandled = CyTrue;
        }
        if (request == 0x76)
        {
            /* When operating at USB 2.0 speeds, there is a possibility that the bulk transfers create
               errors during control transfers. Ensure that the bulk channel is suspended for the duration
               of the control request to avoid this possibility. We use a timeout of 1 second to ensure
               that the control pipe does not get stuck in the case where the host is not reading the
               bulk pipe. */
            glEp0Buffer[0] = glVendorRequestCount++;
            glEp0Buffer[1] = glUnderrunCount;
            glEp0Buffer[2] = 1;
            glEp0Buffer[3] = 2;
            CyU3PUsbSendEP0Data(length, glEp0Buffer);
            isHandled = CyTrue;
        }
    }

    if (type == CY_U3P_USB_STANDARD_RQT)
    {
        if ((target == CY_U3P_USB_TARGET_INTF) && ((request == CY_U3P_USB_SC_SET_FEATURE)
                    || (request == CY_U3P_USB_SC_CLEAR_FEATURE)) && (value == 0))
        {
            if (glIsApplicationActive)
            {
                CyU3PUsbAckSetup ();
                glForceLinkU2 = (request == CY_U3P_USB_SC_SET_FEATURE);
             }
            else CyU3PUsbStall(0, CyTrue, CyFalse);

            isHandled = CyTrue;
        }

        if ((target == CY_U3P_USB_TARGET_ENDPT) && (request == CY_U3P_USB_SC_CLEAR_FEATURE)
                && (value == CY_U3P_USBX_FS_EP_HALT))
        {
            if (glIsApplicationActive)
            {
                if ((index == USB_CONSUMER_ENDPOINT) || (index == USB_PRODUCER_ENDPOINT))
                {
                    CyU3PUsbStall(index, CyFalse, CyTrue);
                    isHandled = CyTrue;
                    CyU3PUsbAckSetup();
                }
            }
        }
    }
    return isHandled;
}

/**
 * @brief <b> USB Event Callbacks </b><br>
 * Event callbacks needed to support the USB device driver
 *
 * @param Event CyU3PUsbEventType_t
 * @param EventData uint16_t
 */
void USBEvent_Callback(CyU3PUsbEventType_t Event, uint16_t EventData )
{
	CyU3PEventSet(&CallbackEvent, 1<<Event, CYU3P_EVENT_OR);
	switch (Event)
    {
     case CY_U3P_USB_EVENT_CONNECT:
       break;

    case CY_U3P_USB_EVENT_SETCONF:
        /* If the application is already active stop it before re-enabling. */
        if (glIsApplicationActive) StopApplication();
        MAXCLOCKVALUE = (CyU3PUsbGetSpeed() == CY_U3P_SUPER_SPEED) ? 8 : 80;	//	DEBUG at 10MHz with USB 2.0
        ClockValue = MAXCLOCKVALUE;
        StartApplication();
        break;

    case CY_U3P_USB_EVENT_DISCONNECT:
    case CY_U3P_USB_EVENT_RESET:
        glForceLinkU2 = CyFalse;
        if (glIsApplicationActive) StopApplication();
        break;

    case CY_U3P_USB_EVENT_EP_UNDERRUN:
    	glUnderrunCount++;
        CyU3PDebugPrint (7, "\r\nEP Underrun on %d", EventData);
        break;

    default:
        break;
    }
}

/**
 * @brief <b> LPM Callbacks </b><br>
 * Power handling callbacks needed to support the USB device driver
 *
 * @param link_mode CyU3PUsbLinkPowerMode
 * @remarks return true - does nothing
 */
CyBool_t LPMRequest_Callback(CyU3PUsbLinkPowerMode link_mode)
{
    return CyTrue;
}

// Spin up USB, let the USB driver handle enumeration
/**
 * @brief <b> Initialize USB </b><br>
 * USB initialization and the callbacks function are specified
 *
 * @return status CyU3PReturnStatus_t, the status of the GPIF
 */
CyU3PReturnStatus_t InitializeUSB(void)
{
	CyU3PReturnStatus_t Status;
	CyBool_t NeedToRenumerate = CyTrue;

	Status = CyU3PUsbStart();
    if (Status == CY_U3P_ERROR_NO_REENUM_REQUIRED)
    {
    	NeedToRenumerate = CyFalse;
    	Status = CY_U3P_SUCCESS;
    }
	CheckStatus("Start USB Driver", Status);

	// Setup callbacks to handle the setup requests, USB Events and LPM Requests (for USB 3.0)
    CyU3PUsbRegisterSetupCallback(USBSetup_Callback, CyTrue);
    CyU3PUsbRegisterEventCallback(USBEvent_Callback);
    CyU3PUsbRegisterLPMRequestCallback(LPMRequest_Callback);

    // Driver needs all of the descriptors so it can supply them to the host when requested
    Status = SetUSBdescriptors();
    CheckStatus("Set USB Descriptors", Status);

    // Connect the USB Pins with SuperSpeed operation enabled
    if (NeedToRenumerate)
    {
    	Status = CyU3PConnectState(CyTrue, CyTrue);
    	CheckStatus("Connect USB", Status);
    }
    else	// USB connection already exists, restart the Application
    {
        if (glIsApplicationActive) StopApplication();
        StartApplication();
    }

    return Status;
}







