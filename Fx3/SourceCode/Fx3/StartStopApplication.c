/**
 * @file	StartStopApplication.c
 * @brief	The application initialization
 * and configuration for USB, DMA and GPIF are done here
 *
 * @author	Vinurudh Damarla
 *
 */

#include "Application.h"
#include "cyfxgpif2config.h"
char* CyFxGpifConfigName = {"cyfxgpif2config.h"};	/**< File to consider for GPIF */

/**
 * @brief <b> Checks the status </b><br>
 * Checks the status of the API and prints in the debug console
 *
 * @param StringPtr char, name to be printed
 * @param status CyU3PReturnStatus_t, the status returned
 */
extern void CheckStatus(char* StringPtr, CyU3PReturnStatus_t Status);

extern CyBool_t glIsApplicationActive;	/**< flag for application is active or not */
extern uint32_t ClockValue;				/**< clock value */

CyU3PDmaChannel glGPIF2USB_Handle;	/**< DMA for GPIF to USB */
CyU3PDmaChannel glUSB2GPIF_Handle;	/**< DMA for USB to GPIF */

const char* BusSpeed[] = { "Not Connected", "Full ", "High ", "Super" };	/**< ENUM for types of USB */
const uint16_t EpSize[] = { 0, 64, 512, 1024 };	/**< Depending on the type the packet size  */

/**
 * @brief <b> Initialize and start GPIF </b><br>
 * GPIF is initialized and Statemachine is started from the specified state
 *
 * @return status CyU3PReturnStatus_t, the status of the GPIF
 */
CyU3PReturnStatus_t StartGPIF(void)
{
	CyU3PReturnStatus_t Status;
	CyU3PDebugPrint(7, "\r\nUsing %s", CyFxGpifConfigName);
	Status = CyU3PGpifLoad(&CyFxGpifConfig);
	CheckStatus("GpifLoad", Status);

	CyU3PGpifSocketConfigure (0,CY_U3P_PIB_SOCKET_0,6,CyFalse,1);
	CyU3PGpifSocketConfigure (3,CY_U3P_PIB_SOCKET_3,6,CyFalse,1);

	Status = CyU3PGpifSMStart(RESET, ALPHA_RESET);
	return Status;
}

/**
 * @brief <b> Initialize and configure USB, DMA and GPIF start the transfer </b><br>
 */
void StartApplication(void)
{
	CyU3PEpConfig_t epConfig;
	CyU3PDmaChannelConfig_t dmaConfig;
	CyU3PReturnStatus_t Status;
    CyU3PPibClock_t pibClock;

    CyU3PUSBSpeed_t usbSpeed = CyU3PUsbGetSpeed();
    CyU3PDebugPrint(4, "\r\n@StartApplication, running at %sSpeed", BusSpeed[usbSpeed]);
    CyU3PDebugPrint(4, "\r\n@Clock Value for GPIF %d ", ClockValue);

    // Initialize p-port block
    pibClock.clkDiv = 2;
    pibClock.clkSrc = CY_U3P_SYS_CLK;
    pibClock.isHalfDiv = CyFalse;
    pibClock.isDllEnable = CyFalse;		// Disable Dll since this application is synchronous and slave
    Status = CyU3PPibInit(CyTrue, &pibClock);
    CheckStatus("Start GPIF Clock", Status);

    CyU3PDebugPrint(4, "\r\n@Clock Divide and Half Clk Value %d , %d ", pibClock.clkDiv,pibClock.isHalfDiv);
 	CyU3PDebugPrint(4, "\r\nGPIF Clock = %d MHz = %d MB/s", 800/ClockValue, 3200/ClockValue);

    // End Point Configuration
	CyU3PMemSet((uint8_t *)&epConfig, 0, sizeof(epConfig));
	epConfig.enable = CyTrue;
	epConfig.epType = CY_U3P_USB_EP_BULK;
	epConfig.burstLen = (usbSpeed == CY_U3P_SUPER_SPEED) ? 16 : 1;
	epConfig.pcktSize = EpSize[usbSpeed];
	epConfig.streams = 0;

	// Setup and flush the Consumer endpoint
	Status = CyU3PSetEpConfig(USB_CONSUMER_ENDPOINT, &epConfig);
	CheckStatus("CyU3PSetEpConfig_Enable", Status);

	// Setup and flush the producer endpoint
	epConfig.burstLen =  (usbSpeed == CY_U3P_SUPER_SPEED) ? 16 : 1;
	Status = CyU3PSetEpConfig(USB_PRODUCER_ENDPOINT, &epConfig);
	CheckStatus("CyU3PSetEpConfig_Enable", Status);

	// Create a DMA AUTO channel for the GPIF to USB transfer
	CyU3PMemSet((uint8_t *)&dmaConfig, 0, sizeof(dmaConfig));
	dmaConfig.size           = (16 * EpSize[usbSpeed]);
	dmaConfig.count          = 4;
	dmaConfig.dmaMode        = CY_U3P_DMA_MODE_BYTE;

#if STREAM_OUT_ENABLED
	dmaConfig.prodSckId		 = GPIF_PRODUCER_SOCKET;
	dmaConfig.consSckId		 = (CyU3PDmaSocketId_t)USB_CONSUMER_ENDPOINT_SOCKET;
	Status = CyU3PDmaChannelCreate(&glGPIF2USB_Handle, CY_U3P_DMA_TYPE_AUTO, &dmaConfig);
	CheckStatus("DmaChannelCreate", Status);
	Status = CyU3PDmaChannelSetXfer(&glGPIF2USB_Handle, 0);
	CheckStatus("DmaChannelStart GPIF - USB", Status);
	Status = CyU3PUsbFlushEp(USB_CONSUMER_ENDPOINT);
	CheckStatus("CyU3PUsbFlushEp", Status);
#endif

#if STREAM_IN_ENABLED
	dmaConfig.size           = 64;
	dmaConfig.count          = 1;
	dmaConfig.prodSckId		 = USB_PRODUCER_ENDPOINT_SOCKET;
	dmaConfig.consSckId		 = GPIF_CONSUMER_SOCKET;
	Status = CyU3PDmaChannelCreate(&glUSB2GPIF_Handle, CY_U3P_DMA_TYPE_AUTO, &dmaConfig);
	CheckStatus("DmaChannelCreate", Status);
	Status = CyU3PDmaChannelSetXfer(&glUSB2GPIF_Handle, 0);
	CheckStatus("DmaChannelStart USB - GPIF", Status);
	Status = CyU3PUsbFlushEp(USB_PRODUCER_ENDPOINT);
	CheckStatus("CyU3PUsbFlushEp", Status);
#endif

	// Load, configure and start the GPIF state machine
    Status = StartGPIF();
	CheckStatus("GpifStart", Status);

    glIsApplicationActive = CyTrue;
}

/**
 * @brief <b> Deinitialize USB, DMA and GPIF start the transfer </b><br>
 */
void StopApplication(void)
{
    CyU3PEpConfig_t epConfig;
    CyU3PReturnStatus_t Status;

    CyU3PGpifDisable(CyTrue);
    Status = CyU3PPibDeInit();
    CheckStatus("Stop GPIF Block", Status);

#ifdef STREAM_OUT_ENABLED
    Status = CyU3PDmaChannelDestroy(&glGPIF2USB_Handle);
    CheckStatus("DmaChannelDestroy", Status);
    Status = CyU3PUsbFlushEp(USB_CONSUMER_ENDPOINT);
    CheckStatus("FlushEndpoint", Status);
    CyU3PMemSet((uint8_t *)&epConfig, 0, sizeof(&epConfig));
    Status = CyU3PSetEpConfig(USB_CONSUMER_ENDPOINT, &epConfig);
	CheckStatus("SetEndpointConfig_Disable", Status);
#endif

#ifdef STREAM_IN_ENABLED
	Status = CyU3PDmaChannelDestroy(&glUSB2GPIF_Handle);
	CheckStatus("DmaChannelDestroy", Status);
	Status = CyU3PUsbFlushEp(USB_PRODUCER_ENDPOINT);
	CheckStatus("FlushEndpoint", Status);
	CyU3PMemSet((uint8_t *)&epConfig, 0, sizeof(&epConfig));
	Status = CyU3PSetEpConfig(USB_PRODUCER_ENDPOINT, &epConfig);
	CheckStatus("SetEndpointConfig_Disable", Status);
#endif

    glIsApplicationActive = CyFalse;
}

