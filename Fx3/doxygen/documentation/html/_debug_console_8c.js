var _debug_console_8c =
[
    [ "CheckStatus", "_debug_console_8c.html#ae181e6f2f903d19ab39e1393c41a7d87", null ],
    [ "CyU3PDebugPrintEvent", "_debug_console_8c.html#a99f60c49a66cb445aa8e09e8cd82185c", null ],
    [ "DebugPrintEvent", "_debug_console_8c.html#a7b55c9acb804bd95b762e6f483f538e2", null ],
    [ "InitializeDebugConsole", "_debug_console_8c.html#aeeab89cc4418260e7e6d638b02602890", null ],
    [ "ParseCommand", "_debug_console_8c.html#a87676fb07d216da1870d44ea14c8ec55", null ],
    [ "StartApplication", "_debug_console_8c.html#ac7f54e17f692fd846e3f4d61dbf20436", null ],
    [ "StopApplication", "_debug_console_8c.html#ac23eb59fab315c12356c9953fc915d94", null ],
    [ "UartCallback", "_debug_console_8c.html#a16071d6a5532aed3aaa4ac39120847d2", null ],
    [ "CallbackEvent", "_debug_console_8c.html#adad20c24d7e376118d6c52acb076fe3e", null ],
    [ "ClockValue", "_debug_console_8c.html#a031e527ddae0f5380544d436719e970b", null ],
    [ "EventName", "_debug_console_8c.html#a57a3d91a333821344b04a05ddca71406", null ],
    [ "glConsoleInBuffer", "_debug_console_8c.html#a8f0190d049635dbafd1d22722a84f2e2", null ],
    [ "glConsoleInIndex", "_debug_console_8c.html#a7d9be69ee9ad7c5cbaccd707c7dc2394", null ],
    [ "glDebugTxEnabled", "_debug_console_8c.html#a78a92ecd99faebfd7dd769d74bf30b04", null ],
    [ "glUARTtoCPU_Handle", "_debug_console_8c.html#a52cbd698545a2061ab5c7c4fefefb82c", null ],
    [ "MAXCLOCKVALUE", "_debug_console_8c.html#a6a20403f68c4eda5a5cf9cd733ec74a0", null ]
];