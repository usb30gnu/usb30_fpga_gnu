var _u_s_b___handler_8c =
[
    [ "__attribute__", "_u_s_b___handler_8c.html#a578c0c4abc20994b3052ce6f95f12afd", null ],
    [ "CheckStatus", "_u_s_b___handler_8c.html#ae181e6f2f903d19ab39e1393c41a7d87", null ],
    [ "InitializeUSB", "_u_s_b___handler_8c.html#ad2a1c92d01a4933aff64453e45f91368", null ],
    [ "LPMRequest_Callback", "_u_s_b___handler_8c.html#acfcce7fc9c0ac93ef2a3f97c62780ab3", null ],
    [ "SetUSBdescriptors", "_u_s_b___handler_8c.html#ab6384b32f3ea84f3c2b119e7648784fc", null ],
    [ "StartApplication", "_u_s_b___handler_8c.html#ac7f54e17f692fd846e3f4d61dbf20436", null ],
    [ "StopApplication", "_u_s_b___handler_8c.html#ac23eb59fab315c12356c9953fc915d94", null ],
    [ "USBEvent_Callback", "_u_s_b___handler_8c.html#a1f802c0f060688e404b635a050365ce2", null ],
    [ "USBSetup_Callback", "_u_s_b___handler_8c.html#a00a83e8cb7d9b947f9cbe51622b405cf", null ],
    [ "CallbackEvent", "_u_s_b___handler_8c.html#adad20c24d7e376118d6c52acb076fe3e", null ],
    [ "ClockValue", "_u_s_b___handler_8c.html#a031e527ddae0f5380544d436719e970b", null ],
    [ "glChannelSuspended", "_u_s_b___handler_8c.html#a37dde8275b0dfc1a5e55310b30e68246", null ],
    [ "glForceLinkU2", "_u_s_b___handler_8c.html#a888c12ae7a2d3bb3aec1dfbd3b6acf36", null ],
    [ "glIsApplicationActive", "_u_s_b___handler_8c.html#a46adc16a8e7114148a8566acd34a1bb8", null ],
    [ "glUnderrunCount", "_u_s_b___handler_8c.html#ac43664f80c6d79b97193bceca9901b86", null ],
    [ "glVendorRequestCount", "_u_s_b___handler_8c.html#a92e33fc0a3d31fd5ef68512ca3126fa6", null ],
    [ "MAXCLOCKVALUE", "_u_s_b___handler_8c.html#adc98986ef012829f02590cb8dd3b13c6", null ]
];