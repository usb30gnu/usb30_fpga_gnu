var cyfxgpif2config_8h =
[
    [ "ALPHA_RESET", "cyfxgpif2config_8h.html#a76ce82c40a9be4f96ebe17df26da104f", null ],
    [ "CY_NUMBER_OF_STATES", "cyfxgpif2config_8h.html#a5f53dd51084aeb29c7921c5b1dc7e853", null ],
    [ "DSS_STATE", "cyfxgpif2config_8h.html#aaafebe5f89dfcc859cdbcf76e6bb4657", null ],
    [ "IDLE", "cyfxgpif2config_8h.html#a9c21a7caee326d7803b94ae1952b27ca", null ],
    [ "READ", "cyfxgpif2config_8h.html#ada74e7db007a68e763f20c17f2985356", null ],
    [ "RESET", "cyfxgpif2config_8h.html#ab702106cf3b3e96750b6845ded4e0299", null ],
    [ "SHORT_PKT", "cyfxgpif2config_8h.html#a452bb3341d0d6e8c94487d0279a1f4d6", null ],
    [ "WRITE", "cyfxgpif2config_8h.html#aa10f470e996d0f51210d24f442d25e1e", null ],
    [ "ZLP", "cyfxgpif2config_8h.html#a12c0c63363b108936a7fe344c13de6bf", null ],
    [ "CyFxGpifConfig", "cyfxgpif2config_8h.html#a12fecbb5219416863d8ed5bb2d4436e8", null ],
    [ "CyFxGpifRegValue", "cyfxgpif2config_8h.html#a3e512fe39b22d86fa28b19a0e0938d57", null ],
    [ "CyFxGpifTransition", "cyfxgpif2config_8h.html#a24bf6cb1d6c09479494f1e68e29f3d79", null ],
    [ "CyFxGpifWavedata", "cyfxgpif2config_8h.html#a4f1f5577ffc028b24d967ef4a186e326", null ],
    [ "CyFxGpifWavedataPosition", "cyfxgpif2config_8h.html#afc4657c87ca26f20b08be388d1e29276", null ]
];