var _supply_gpif_data_8h =
[
    [ "ALPHA_START", "_supply_gpif_data_8h.html#a73fac37213dc71a929ff706ddeeb6b72", null ],
    [ "CY_NUMBER_OF_STATES", "_supply_gpif_data_8h.html#a5f53dd51084aeb29c7921c5b1dc7e853", null ],
    [ "SAVE", "_supply_gpif_data_8h.html#ab2868d85c8a74e3ee08bdc44580e2235", null ],
    [ "START", "_supply_gpif_data_8h.html#a3018c7600b7bb9866400596a56a57af7", null ],
    [ "WAIT", "_supply_gpif_data_8h.html#aef72fe86b7c60b1a86920496456edeac", null ],
    [ "CyFxGpifConfig", "_supply_gpif_data_8h.html#a12fecbb5219416863d8ed5bb2d4436e8", null ],
    [ "CyFxGpifRegValue", "_supply_gpif_data_8h.html#a3e512fe39b22d86fa28b19a0e0938d57", null ],
    [ "CyFxGpifTransition", "_supply_gpif_data_8h.html#a24bf6cb1d6c09479494f1e68e29f3d79", null ],
    [ "CyFxGpifWavedata", "_supply_gpif_data_8h.html#a4f1f5577ffc028b24d967ef4a186e326", null ],
    [ "CyFxGpifWavedataPosition", "_supply_gpif_data_8h.html#afc4657c87ca26f20b08be388d1e29276", null ]
];