# FPGA BASED INTERFACING OF USB 3.0 IN GNU RADIO FRAMEWORK

The objective of the project is to provide a high-speed interface between Host PC and FPGA using Superspeed USB 3.0 feature of Cypress FX3 and visualize the data in GNU Radio. 
The GNU Radio shall transmit the start bit to FPGA; on receiving the start bits the FPGA shall transmit the bulk data to GNU Radio. 
The communication between FPGA and GNU Radio is established with the FX3. 
To ensure no data loss at FPGA signal tap analysis is done. The achieved throughput is 3.2Gbps (400 Mbps) and the zero loss data is verified by using �File Sink� to store the data and Matlab for plotting the wave

## Getting Started
### System Requirements
 * Hardware - HSMC interconnect board - CYUSB3ACC-003, FPGA board - Cyclone V GX Starter Kit, Cypress EZ-USB 3.0 FX3 SuperSpeed Explorer Kit (CYUSB3KIT-003)
 * Software - Quartus Prime lite Edition 16.1, Cypress Control Center, Cypress Streamer, GPIF II Designer, ubuntu-16.04.2-desktop-i386, VirtualBox-5.1.22-115126-Win, Gnu Radio Companion, Microsoft Visual Studio 2015 / Eclipse
 
## Implementation Details
The folder "FPGA" has the implementation for the FPGA which includes Stream_IN,Stream_OUT, bidrectional
The folder "FX3" has the implementation for the FX3 - firmware / application, GPIF Statmachine
The folder "GNU Radio" has the implementaion for the USB driver implementation in LINUX and Windows, adding the custom block in GNU Radio
The folder "ProjectReport" contains the project report for more better understanding.

## Project Group
### Professor
Prof. Dr. Christian Jakob

### Members
Mohan Kumar Kotgire, 
Vinurudh Damarla,
Vivek Subramanian, 
Namitha Jayakrishnan, 
Akanksha Singh.

