`timescale 1ns/1ps

`define HALF_CLOCK_PERIOD 10
`define RESET_PERIOD 300
`define FLAGA_PERIOD 400
`define FLAGB_PERIOD 500
`define SIM_DURATION 200000


module Slave_FIFO_Write_tb();
   wire [31:0] tb_q;
	//wire [31:0] tb_test;
	wire clk_out;
	wire slwr;
	wire slrd;
	wire [1:0] faddr;
	wire sloe;
	wire slcs;
	wire pktend;
	wire [1:0] PMODE;
	wire RESET;

// ### clock generation process ###

reg tb_local_clock = 0;

initial
	begin: clock_generation_process
		tb_local_clock = 0;
		forever
			#`HALF_CLOCK_PERIOD tb_local_clock = ~tb_local_clock;
		end
	
//### active low reset generation process ###

reg tb_local_reset = 1;
reg tb_local_flaga = 0;
reg tb_local_flagb = 0;


initial
	begin : reset_generation_process
	$display ("Simulation starts .......");
	
	#`RESET_PERIOD tb_local_reset = 1'b0;
	#`FLAGA_PERIOD tb_local_flaga = 1'b1;
	#`FLAGB_PERIOD tb_local_flagb = 1'b1;
	#10
	#10 tb_local_flaga = 1'b0;
	#10 tb_local_flagb = 1'b0;
	
	#20 tb_local_flaga = 1'b1;
	tb_local_flagb = 1'b1;
	#`SIM_DURATION
	$stop();
	end
	
	


Slave_FIFO_Write  streamIN_inst_0( .reset_in_(tb_local_reset),
										.clk(tb_local_clock),
										.fdata(tb_q),
										//.data_gen_stream_test(tb_test),
										.faddr(faddr),
										.slrd(slrd),
										.slwr(slwr),
										.flaga(tb_local_flaga),
										.flagb(tb_local_flagb),
										.flagc(1'b1),
										.flagd(1'b0),
										.sloe(sloe),
										.clk_out(clk_out),
										.slcs(slcs),
										.pktend(pktend),
										.PMODE(PMODE),
										.RESET(RESET)
										);
endmodule