module Slave_FIFO_Write(
	input  reset_in_,            //input reset active low
	input  clk,                  //input clock 50 Mhz
	inout  [31:0]data,          //32 bit data bus
	output [1:0]addr,           //output 2 bit fifo address  
	output slrd,                 //output read select
	output slwr,                 //output write select
	input  flaga,                //full flag
	input  flagb,                //partial full flag
    input  flagc,                //empty flag
	input  flagd,                //empty partial flag
	output sloe,                 //output enable select
	output clk_out,              //output clk 100 Mhz
	output slcs,                 //output chip select
	output pktend,               //output pkt end
	output [1:0]PMODE,
	output RESET
);


reg [2:0]current_state;
reg [2:0]next_state;
reg [31:0]data_gen_write;
reg slwr_write_temp;

//parameters for slave fifo write mode state machine
parameter [2:0] idle_state                    = 3'd0;
parameter [2:0] wait_flagb_state              = 3'd1;
parameter [2:0] write_state                   = 3'd2;
parameter [2:0] write_delay_state             = 3'd3;


reg  flaga_d;
reg  flagb_d;
reg  flagc_d;
reg  flagd_d;


//output signal assignment 
assign slrd   = 1'b1;
assign slwr   = slwr_write_temp;   
assign addr  = 2'd0;
assign sloe   = 1'b1;
assign data  = (slwr_write_temp) ? 32'dz : data_gen_write;	
assign PMODE  = 2'b11;		
assign RESET  = 1'b1;	
assign slcs   = 1'b0;
assign pktend = 1'b1;

wire lock;
wire reset_;


//PLL to generate 100 MHz clock from the FPGA internal 50Mhz clock
Slave_FIFO_FX3_PLL  inst_clk_pll(
		.refclk(clk),   //  refclk.clk
		.rst(1'b0),      //   reset.reset
		.outclk_0(clk_out), // outclk0.clk
		.locked(lock)   //  locked.export
	);



assign reset_ = lock;

///update of DMA buffer status flags
always @(posedge clk_out, negedge reset_)begin
	if(!reset_)begin 
		flaga_d <= 1'd0;
		flagb_d <= 1'd0;
		flagc_d <= 1'd0;
		flagd_d <= 1'd0;
	end else begin
		flaga_d <= flaga;
		flagb_d <= flagb;
		flagc_d <= flagc;
		flagd_d <= flagd;
	end	
end

assign slwr_write = ((current_state == write_state)) ? 1'b0 : 1'b1;


always @(posedge clk_out, negedge reset_)begin
	if(!reset_)begin 
		slwr_write_temp <= 1'b1;
	end else begin
		slwr_write_temp <= slwr_write;
	end	
end


//write mode state machine
always @(posedge clk_out, negedge reset_)begin
	if(!reset_)begin 
		current_state <= idle_state;
	end else begin
		current_state <= next_state;
	end	
end

//write mode state machine combinational logic
always @(*)begin
	next_state = current_state;
	case(current_state)
	idle_state:begin
		if(flaga_d == 1'b1)begin
			next_state = wait_flagb_state; 
		end else begin
			next_state = idle_state;
		end	
	end
	wait_flagb_state :begin
		if (flagb_d == 1'b1)begin
			next_state = write_state; 
		end else begin
			next_state = wait_flagb_state; 
		end
	end
	write_state:begin
		if(flagb_d == 1'b0)begin
			next_state = write_delay_state;
		end else begin
		 	next_state = write_state;
		end
	end
        write_delay_state:begin
			next_state = idle_state;
	end
	endcase
end

//32bit counter data generator  
always @(posedge clk_out, negedge reset_)begin
	if(!reset_)begin 
		data_gen_write <= 32'd0;
	end else if(slwr_write_temp == 1'b0) begin
		data_gen_write <= data_gen_write + 1;
	end 
end


endmodule
