`timescale 1ns/1ps

`define HALF_CLOCK_PERIOD 10
`define FDATA_INCR_PERIOD 10
`define RESET_PERIOD 300
`define FLAGA_PERIOD 400
`define FLAGB_PERIOD 500
`define SIM_DURATION 200000


module Slave_FIFO_Read_tb();
   //wire [31:0] tb_q;
	//wire [31:0] tb_test;
	wire clk_out;
	wire slwr;
	wire slrd;
	wire [1:0] faddr;
	wire sloe;
	wire slcs;
	wire pktend;
	wire [1:0] PMODE;
	wire RESET;
	wire [7:0]  LEDG;
	wire [9:0]  LEDR;

// ### clock generation process ###

reg tb_local_clock = 0;

initial
	begin: clock_generation_process
		tb_local_clock = 0;
		forever
			#`HALF_CLOCK_PERIOD tb_local_clock = ~tb_local_clock;
		end
		
//data generation that to be written to FPGA

reg [31:0] tb_local_fdata = 0;

initial
	begin: fdata_generation_process
		tb_local_fdata = 0;
		forever
			#`FDATA_INCR_PERIOD tb_local_fdata = tb_local_fdata+1'b1;
		end
	
//### active low reset generation process ###

reg tb_local_reset = 1;
reg tb_local_flagc = 0;
reg tb_local_flagd = 0;


initial
	begin : reset_generation_process
	$display ("Simulation starts .......");
	
	#`RESET_PERIOD tb_local_reset = 1'b0;
	#`FLAGA_PERIOD tb_local_flagc = 1'b1;
	#`FLAGB_PERIOD tb_local_flagd = 1'b1;
	#20
	#20 tb_local_flagc = 1'b0;
	#20 tb_local_flagd = 1'b0;
	#40 tb_local_flagc = 1'b1;
	tb_local_flagd = 1'b1;
	#`SIM_DURATION
	$stop();
	end
	
	


Slave_FIFO_Read  streamIN_inst_0( .reset_in_(tb_local_reset),
										.clk(tb_local_clock),
										.fdata(tb_local_fdata),
										//.data_gen_stream_test(tb_test),
										.faddr(faddr),
										.slrd(slrd),
										.slwr(slwr),
										.flaga(1'b0),
										.flagb(1'b0),
										.flagc(tb_local_flagc),
										.flagd(tb_local_flagd),
										.sloe(sloe),
										.clk_out(clk_out),
										.slcs(slcs),
										.pktend(pktend),
										.PMODE(PMODE),
										.RESET(RESET),
										.LEDG(LEDG),
										.LEDR(LEDR)
										);
endmodule