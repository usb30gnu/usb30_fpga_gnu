transcript on
if ![file isdirectory Slave_FIFO_Read_iputf_libs] {
	file mkdir Slave_FIFO_Read_iputf_libs
}

if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

###### Libraries for IPUTF cores 
###### End libraries for IPUTF cores 
###### MIF file copy and HDL compilation commands for IPUTF cores 


vlog "E:/Mohan_learning/A_FPGA_USB/compare/All_code_folder/with_modelsim/Slave_FIFO_Read/Slave_FIFO_FX3_PLL_sim/Slave_FIFO_FX3_PLL.vo"

vlog -vlog01compat -work work +incdir+E:/Mohan_learning/A_FPGA_USB/compare/All_code_folder/with_modelsim/Slave_FIFO_Read {E:/Mohan_learning/A_FPGA_USB/compare/All_code_folder/with_modelsim/Slave_FIFO_Read/Slave_FIFO_Read.v}

vlog -vlog01compat -work work +incdir+E:/Mohan_learning/A_FPGA_USB/compare/All_code_folder/with_modelsim/Slave_FIFO_Read {E:/Mohan_learning/A_FPGA_USB/compare/All_code_folder/with_modelsim/Slave_FIFO_Read/Slave_FIFO_Read_tb.v}

vsim -t 1ps -L altera_ver -L lpm_ver -L sgate_ver -L altera_mf_ver -L altera_lnsim_ver -L cyclonev_ver -L cyclonev_hssi_ver -L cyclonev_pcie_hip_ver -L rtl_work -L work -voptargs="+acc"  Slave_FIFO_Read_tb

add wave *
view structure
view signals
run -all
