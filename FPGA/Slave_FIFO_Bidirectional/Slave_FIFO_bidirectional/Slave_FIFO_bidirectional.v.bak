module Slave_FIFO_bidirectional#(parameter DATA_WIDTH = 16, parameter ADDR_WIDTH = 8)(
	input  flaga,               //empty flag from FX3 to initiate write operation 
	input  flagb, 				//empty partial flag from FX3 to initiate write operation
	input  flagc,               //full flag from FX3 to initiate read operation
	input  flagd,               //full flag from FX3 to initiate read operation
	output slrd,                //output read select
	output slwr,                //output write select
	output sloe,                //output output enable select
	output slcs,                //output chip select
	output pktend,              //output pkt end	
	input  reset_in_,           //input reset active low
	output RESET,					 //output reset
	input  clk,                 //input clock 50 Mhz
	output clk_out,             //output clk 100 Mhz 
	//output clk_pll_400,          //output clk 100 Mhz and 180 phase shift
	inout [31:0]fdata,          //data bus
//input [31:0]datain_tb,
	output [1:0]addr,            //output 2-bit fifo address  
   output [1:0]PMODE
); 


reg [1:0] watermark_delay_cnt;	
reg rd_watermark_delay_cnt; 

reg [31:0]data_out;
reg [31:0]data_out_d;
reg [31:0] data_in;

reg slrd_delay1, slrd_delay2, slrd_delay3, slrd_delay4;
wire slrd_mode, sloe_mode, slwr_delay1;
reg flaga_d, flagb_d,flagc_d,flagd_d;

reg [1:0]fifo_address;
reg [1:0]fifo_address_d;

reg initiate_write  = 1'b0;

reg [3:0]current_state;
reg [3:0]next_state;

//parameters for bidirectional mode state machine
parameter [3:0] idle_state                    = 4'd0;
parameter [3:0] flagc_rcvd_state              = 4'd1;
parameter [3:0] wait_flagd_state              = 4'd2;
parameter [3:0] read_state                    = 4'd3;
parameter [3:0] read_watermark_delay_state    = 4'd4;
parameter [3:0] wait_watermark_delay_state    = 4'd5;
parameter [3:0] check_for_write_state         = 4'd6;
parameter [3:0] wait_flaga_state              = 4'd7;
parameter [3:0] wait_flagb_state              = 4'd8;
parameter [3:0] write_state                   = 4'd9;
parameter [3:0] write_wr_delay_state          = 4'd10;

//output signal assignment

assign slrd   = slrd_mode;
assign slwr   = slwr_delay1;   
assign addr  = fifo_address_d;
assign sloe   = sloe_mode;
assign PMODE  = 2'b11;		
assign reset_out  = 1'b1;	
assign slcs   = 1'b0;
assign pktend = 1'b1;

// Declare the ROM variable
reg [DATA_WIDTH-1:0] rom[0:2**ADDR_WIDTH-1];

reg [8:0] addr_sine;

initial
  begin
		$readmemh("sine_wave.txt", rom);
  end
  
  
 always@(posedge clk_out, negedge reset_) begin
    if(!reset_) begin
		addr_sine<= 8'd0;
	end
	 else if(slwr_delay1 == 1'b0) begin
	   addr_sine<= (addr_sine == 8'd255)? 8'd0:(addr_sine + 1'b1);
	end
	end


//assign clk_out = clk_in;        //for TB

altpll_fx3 inst_clk_pll
	(
		
		.refclk(clk_in),  
		.rst(1'b0/*reset2pll*/),
		.outclk_0(clk_out),
		.outclk_1(clk_pll_400), //180 deg phase shifted 100MHz clock
		.locked(lock)
	);


////ddr is used to send out the clk_in(DDR instantiation)
//
//ddr inst_ddr_to_send_clk_to_fx3                       
//        ( 
//	.datain_h(1'b0),
//	.datain_l(1'b1),
//	.outclock(clk_100),
//	.dataout(clk_out) 
//	); 
 

assign reset_ = lock;

///flopping the INPUTs flags
always @(posedge clk_out, negedge reset_)begin
	if(!reset_)begin 
		flaga_d <= 1'd0;
		flagb_d <= 1'd0;
		flagc_d <= 1'd0;
		flagd_d <= 1'd0;
	end else begin
		flaga_d <= flaga;
		flagb_d <= flagb;
		flagc_d <= flagc;
		flagd_d <= flagd;
	end	
end

// output control signal generation based on the state machine
assign slrd_mode      = ((current_state == read_state) | (current_state == read_watermark_delay_state)) ? 1'b0 : 1'b1;
assign sloe_mode      = ((current_state == read_state) | (current_state == read_watermark_delay_state) | (current_state == wait_watermark_delay_state)) ? 1'b0 : 1'b1; 
assign slwr_mode      = ((current_state == write_state)) ? 1'b0 : 1'b1;

//delay for writing into slave fifo
always @(posedge clk_out, negedge reset_)begin
	if(!reset_)begin 
		slwr_delay1 <= 1'b1;
	end else begin
		slwr_delay1 <=  slwr_mode;
	end	
end


//delay for reading from slave fifo(data will be available after two clk_in cycle) 
always @(posedge clk_out, negedge reset_)begin
	if(!reset_)begin 
		slrd_delay1 <= 1'b1;
		slrd_delay2 <= 1'b1;  
		slrd_delay3 <= 1'b1;
		slrd_delay4 <= 1'b1;
 	end else begin
 		slrd_delay1 <= slrd_mode;
		slrd_delay2 <= slrd_delay1; 
		slrd_delay3 <= slrd_delay2;
		slrd_delay4 <= slrd_delay3;
	end	
end

// getting the input data
always @(posedge clk_out, negedge reset_)begin
	if(!reset_)begin 
		data_in <= 32'd0;
 	end else begin
		data_in <= data_inout;
	end	
end

///slave fifo address
always@(*)begin
	if((current_state == flagc_rcvd_state) | (current_state == wait_flagd_state) | (current_state == read_state) | (current_state == read_watermark_delay_state) | (current_state == wait_watermark_delay_state))begin
		fifo_address = 2'b11;
	end else 
		fifo_address = 2'b00;
end	

//flopping the output fifo address
always @(posedge clk_out, negedge reset_)begin
	if(!reset_)begin 
		fifo_address_d <= 2'd0;
 	end else begin
		fifo_address_d <= fifo_address;
	end	
end

//counter to delay the read because of watermark 
always @(posedge clk_out, negedge reset_)begin
	if(!reset_)begin 
		rd_watermark_delay_cnt <= 1'b0;
	end else if(current_state == read_state) begin
		rd_watermark_delay_cnt <= 1'b1;
        end else if((current_state == read_watermark_delay_state) & (rd_watermark_delay_cnt > 1'b0))begin
		rd_watermark_delay_cnt <= rd_watermark_delay_cnt - 1'b1;
	end else begin
		rd_watermark_delay_cnt <= rd_watermark_delay_cnt;
	end	
end

//Counter to delay for watermark time
always @(posedge clk_out, negedge reset_)begin
	if(!reset_)begin 
		watermark_delay_cnt <= 2'd0;
	end else if(current_state == read_watermark_delay_state) begin
		watermark_delay_cnt <= 2'd2;
        end else if((current_state == wait_watermark_delay_state) & (watermark_delay_cnt > 1'b0))begin
		watermark_delay_cnt <= watermark_delay_cnt - 1'b1;
	end else begin
		watermark_delay_cnt <= watermark_delay_cnt;
	end	
end


//Bidirectional state machine
always @(posedge clk_out, negedge reset_)begin
	if(!reset_)begin 
		current_state <= idle_state;
	end else begin
		current_state <= next_state;
	end	
end

//bidirectional mode state machine 
always @(*)begin
	next_state = current_state;
	case(current_state)
	idle_state:
		begin
			if(flagc_d == 1'b1)
				begin
					next_state = flagc_rcvd_state;
				end 
			else if(initiate_write  > 1'b0)
				begin
					next_state = wait_flaga_state;
				end
			else
				begin
					next_state = idle_state;
				end
		end	
    flagc_rcvd_state:
		begin
			next_state = wait_flagd_state;
		end	
	wait_flagd_state:
		begin
			if(flagd_d == 1'b1)
				begin
					next_state = read_state;
				end
			else	 
				begin		
				next_state = wait_flagd_state;
				end
			end	
    read_state :
		begin
			if(flagd_d == 1'b0)
				begin
					next_state = read_watermark_delay_state;
				end 
			else 
				begin
					next_state = read_state;
				end
			end
	read_watermark_delay_state : 
		begin
			if(rd_watermark_delay_cnt == 1'b0)
			begin
				next_state = wait_watermark_delay_state;
			end 
		else 
			begin
				next_state = read_watermark_delay_state;
			end
		end
    wait_watermark_delay_state : 
		begin
		if(watermark_delay_cnt == 1'b0)
			begin
				next_state = check_for_write_state;
			end 
		else 
			begin
				next_state = wait_watermark_delay_state;
			end
		end
	check_for_write_state:
		begin
		if(data_in == 32'b11111)	
			begin
				next_state = wait_flaga_state;
				initiate_write  = 1'b1;
			end 
//		else if(data_in == 32'b0)
//			begin
//				next_state = idle_state;
//				initiate_write =0;
//			end
		else
			begin
				next_state = check_for_write_state;
			end
		end	
	wait_flaga_state :
		begin
			if (flaga_d == 1'b1)
				begin
					next_state = wait_flagb_state; 
				end 
			else 
				begin
					next_state = wait_flaga_state; 
				end
			end
	wait_flagb_state :
		begin
			if (flagb_d == 1'b1)
				begin
					next_state = write_state; 
				end 
			else 
				begin
				next_state = wait_flagb_state; 
			end
		end
	write_state:
		begin
			if(flagc_d == 1'b1)
				begin
					next_state = flagc_rcvd_state;
				end 
			else if(flagb_d == 1'b0)
				begin
					next_state = write_wr_delay_state;
				end 
			else
				begin
				next_state = write_state;
				end
			end
    write_wr_delay_state:
		begin
			next_state = idle_state;
		end
	endcase
end


//data generator counter for StreamIN modes
always @(posedge clk_out, negedge reset_)begin
	if(!reset_)begin 
		data_out_d <= 32'd0;
	end else if(slwr_delay1 == 1'b0) begin
		data_out_d <= rom[addr_sine];
	end 
end	

always @(posedge clk_out, negedge reset_)
begin
	if(!reset_)begin 
		data_out <= 32'd0;
	end 
	else if (slrd_mode==1'b0) begin
		data_out <= data_inout;
	end 
	else if(slwr_delay1==1'b0) begin
		data_out <= data_out_d;
	end 
end

assign data_inout = (slwr_delay1) ? 32'dz : data_out;
endmodule
